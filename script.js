class Employee {
    constructor(name, age, salary) {
      this.name = name;
      this.age = age;
      this.salary = salary;
    }
  
    get name() {
      return this._name;
    }
  
    set name(value) {
      this._name = value;
    }
  
    get age() {
      return this._age;
    }
  
    set age(value) {
      this._age = value;
    }
  
    get salary() {
      return this._salary;
    }
  
    set salary(value) {
      this._salary = value;
    }
  }
  
  class Programmer extends Employee {
    constructor(name, age, salary, lang) {
      super(name, age, salary);
      this.lang = lang;
    }
  
    get salary() {
      return this._salary * 3;
    }
  
    set salary(value) {
      this._salary = value;
    }
  }
  
  const programmer1 = new Programmer('Andrew', 30, 5000, ['JavaScript', 'Python']);
  const programmer2 = new Programmer('Pasha', 35, 6000, ['Java', 'C++']);
  
  console.log(programmer1);
  console.log(programmer2);
  